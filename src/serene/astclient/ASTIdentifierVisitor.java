/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package serene.astclient;

import serene.model.TokenModel;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.TextElement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

/**
 *
 * @author ESTEBAN
 */
public class ASTIdentifierVisitor extends ASTVisitor {

    private TokenModel<String> identifierTokenModel;

    /**
     * Creates a new ASTIdentifierVisitor
     */
    public ASTIdentifierVisitor() {
        identifierTokenModel = new TokenModel();
    }

    /**
     *
     * @return
     */
    public TokenModel<String> getIdentifierTokenModel() {
        return identifierTokenModel;
    }

    /**
     * Visits a TextElement and adds its value to the visitor's identifiers set
     *
     * @param node TextElement to visit
     * @return True if the node was succesfully visited, false otherwise
     */
    @Override
    public boolean visit(TextElement node) {
        String identifier = node.getText();
        return true;
    }

    /**
     * Visits a StringLiteral and adds its value to the visitor's identifiers
     * set
     *
     * @param node StringLiteral to visit
     * @return True if the node was succesfully visited, false otherwise
     */
    @Override
    public boolean visit(StringLiteral node) {
        String identifier = node.getEscapedValue();
        identifierTokenModel.addToken(identifier);
        return true;
    }

    /**
     * Visits a CharacterLiteral and adds its value to the visitor's identifiers
     * set
     *
     * @param node CharacterLiteral to visit
     * @return True if the node was succesfully visited, false otherwise
     */
    @Override
    public boolean visit(CharacterLiteral node) {
        String identifier = node.getEscapedValue();
        identifierTokenModel.addToken(identifier);
        return true;
    }

    /**
     * Visits a VariableDeclarationFragment and adds its name to the visitor's
     * identifiers set
     *
     * @param node VariableDeclarationFragment to visit.
     * @return True if the node was succesfully visited, false otherwise
     */
    @Override
    public boolean visit(VariableDeclarationFragment node) {
//        identifiers.add(node.getName().toString());
        return true;
    }

    /**
     * Visits a MethodDeclaration and adds its name to the visitor's identifiers
     * set
     *
     * @param node MethodDeclaration to visit
     * @return True if the node was succesfully visited, false otherwise
     */
    @Override
    public boolean visit(MethodDeclaration node) {
        String identifier = node.getName().toString();
        identifierTokenModel.addToken(identifier);
        return true;
    }

    /**
     * Visits a TypeDeclaration and adds its name to the visitor's identifiers
     * set
     *
     * @param node TypeDeclaration to visit
     * @return True if the node was succesfully visited, false otherwise
     */
    @Override
    public boolean visit(TypeDeclaration node) {
        String identifier = node.getName().toString();
        identifierTokenModel.addToken(identifier);
        return true;
    }
}
