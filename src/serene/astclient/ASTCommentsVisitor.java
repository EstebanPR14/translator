    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package serene.astclient;

import java.util.TreeSet;
import serene.model.TokenModel;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.BlockComment;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.LineComment;

/**
 *
 * @author ESTEBAN
 */
public class ASTCommentsVisitor extends ASTVisitor {

//    private ArrayList<String> comments;
    private TokenModel<String> commentsTokenModel; 
    private String source;

    /**
     * Creates a new ASTCommentsVisitor object
     */
    public ASTCommentsVisitor() {
    }

    /**
     * Creates a new ASTCommentsVisitor object
     *
     * @param source source from which the AST is contructed
     */
    public ASTCommentsVisitor(String source) {
        commentsTokenModel = new TokenModel<>();
        this.source = source;
    }

    /**
     *
     * @return
     */
    public TokenModel<String> getCommentsTokenModel() {
        return commentsTokenModel;
    }

    /**
     * Visits a BlockComment and adds its value to the visitor's comments set
     *
     * @param node BlockComment to visit
     * @return True if the node was succesfully visited, false otherwise
     */
    @Override
    public boolean visit(BlockComment node) {
        String comment = node.toString();
        commentsTokenModel.addToken(comment);
        return true;
    }

    /**
     * Visits a Javadoc and adds its value to the visitor's comments set
     *
     * @param node Javadoc to visit
     * @return True if the node was succesfully visited, false otherwise
     */
    @Override
    public boolean visit(Javadoc node) {
        String comment = node.toString();
        commentsTokenModel.addToken(comment);
        return true;
    }

    /**
     * Visits a LineComment and adds its value to the visitor's comments set
     *
     * @param node LineComment to visit
     * @return True if the node was succesfully visited, false otherwise
     */
    @Override
    public boolean visit(LineComment node) {
        int start = node.getStartPosition();
        int end = start + node.getLength();
        String comment = source.substring(start, end);
        commentsTokenModel.addToken(comment.toString());
        return true;
    }
}
