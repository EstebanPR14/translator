/**
 * 
 */
package serene.astclient;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.TreeSet;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.Comment;
import org.eclipse.jdt.core.dom.CompilationUnit;

import serene.model.TokenModel;
import serene.translator.Control.FileManager;
import serene.translator.Control.StringProcessor;
//import org.ec
/**
 * @author JavierRicardo
 * @author USER
 *
 */
public class ASTClient {

	 /**
    *
    * @param file
    * @param compilationUnit
    * @return
    * @throws IOException
    */
   @SuppressWarnings("unchecked")
public TokenModel<String> retrieveComments(File file, CompilationUnit compilationUnit) throws IOException {
       String source = new FileManager().getString(file);
       ASTCommentsVisitor visitor = new ASTCommentsVisitor(source);

       for (Comment comment : (List<Comment>) compilationUnit.getCommentList()) {
           comment.accept(visitor);
       }
       compilationUnit.accept(visitor);
       return visitor.getCommentsTokenModel();
   }

   /**
    * Method that process a given java file and retrieves the comments and
    * other relevant information
    *
    * @param file
    * @return
    * @throws IOException when there's an error reading the file
    */
   public TokenModel<String> retrieveIdentifiers(File file, CompilationUnit compilationUnit) throws IOException {
       ASTIdentifierVisitor visitor = new ASTIdentifierVisitor();
       compilationUnit.accept(visitor);
       return visitor.getIdentifierTokenModel();
   }

   /**
    * Method that process a given java file and retrieves the comments and
    * identifiers information as tokens
    *
    * @param file
    * @return
    * @throws IOException when there's an error reading the file
    */
   public TokenModel<String> retrieveTokens(File file) throws IOException {

       System.out.println("enter");
       ASTParser parser = ASTParser.newParser(AST.JLS3);
       FileManager fileManager = new FileManager();
       StringProcessor stringProcessor = new StringProcessor();
       String source = fileManager.getString(file);
       parser.setSource(source.toCharArray());
       final CompilationUnit compilationUnit = (CompilationUnit) parser.createAST(null);
       compilationUnit.recordModifications();
       TokenModel<String> content = new TokenModel<>();
//Retrieves comments
       TokenModel<String> comments = retrieveComments(file, compilationUnit);
       
//removes special characters
       for (String comment : comments.getTokens()) {
           String newComment = stringProcessor.replaceSpecialCharacters(comment);
           Integer commentCount = comments.getTokenCount().get(comment);
           content.addToken(newComment, commentCount);
       }

//Splits the content from the comments into single tokens (words)
       TokenModel<String> contentAsTokens = stringProcessor.retrieveWords(content.getTokens());
       //System.out.println("CAT: " + contentAsTokens);
//Retrieves splitted identifiers and removes any identifier name that was present in the comments
       TokenModel<String> identifiers = retrieveIdentifiers(file, compilationUnit);

       for (String identifier : identifiers.getTokens()) {
           TreeSet<String> identifierTokens = stringProcessor.splitIdentifier(identifier);
         //  System.out.println("id: " + identifier + " --> " + identifierTokens + " - " + identifierTokens.size());
           if (identifierTokens.size() > 0) {
               contentAsTokens.addTokens(identifierTokens);
           }
       }
       return contentAsTokens;
   }
}