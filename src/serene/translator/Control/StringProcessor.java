/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package serene.translator.Control;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.TreeSet;
import serene.model.TokenModel;

/**
 *
 * @author ESTEBAN
 */
public class StringProcessor {

    /**
     * Creates a new StringProcessor object
     */
    public StringProcessor() {
    }

    /**
     * Method to remove special characters from a given string
     *
     * @param string string from which special character are to be removed
     * @return The given string whout special characters
     */
    public String removeSpecialCharacters(String string) {
        string = Normalizer.normalize(string, Normalizer.Form.NFD).replaceAll("[^a-zA-Z ]", "");
        string = string.replaceAll("\"", "");
        return string;
    }

    /**
     * Method to replace special characters from a given string with blank
     * spaces
     *
     * @param string string from which special character are to be removed
     * @return The given string whout special characters
     */
    public String replaceSpecialCharacters(String string) {
        string = Normalizer.normalize(string, Normalizer.Form.NFD).replaceAll("[^a-zA-Z ]", " ");
        return string;
    }

    /**
     * Method that splits an identifier.<br>
     * Example: <br>
     * identifier --> [identifier]<br>
     * myIdentifier --> [Identifier, my]<br>
     * Identifier --> [Identifier]<br>
     * my_Identifier --> [Identifier, my]<br>
     * my_identifier --> [identifier, my]<br>
     * my_IDENTIFIER --> [IDENTIFIER, my]<br>
     *
     * @param identifier identifier to be splitted
     * @return A set of strings. A set containing only the original identifier
     * if it cannot be separated
     */
    public TreeSet<String> splitIdentifier(String identifier) {
        identifier = identifier.replaceAll("\\n", "");
        
        ArrayList<String> splittedIdentifierList = new ArrayList<>();
        
        if (identifier.contains(" ")) {
            splittedIdentifierList.addAll(splitIntoWords(identifier));
        }else{
            splittedIdentifierList.addAll(Arrays.asList(identifier.split("_")));
            splittedIdentifierList.addAll(Arrays.asList(identifier.split("(?=\\p{Upper})")));
        }
        
        for (int i = 0; i < splittedIdentifierList.size(); i++) {
            String token = splittedIdentifierList.get(i);
            token = removeSpecialCharacters(token);
            splittedIdentifierList.set(i, token);
            if (token.length() <= 1) {
                splittedIdentifierList.remove(token);
                i--;
            }
        }

        if (splittedIdentifierList.size() >= 1) {
            splittedIdentifierList.remove(identifier);
        } else {
            splittedIdentifierList.add(identifier);
        }
        splittedIdentifierList.remove("");

        TreeSet<String> splittedIdentifier = new TreeSet<>(splittedIdentifierList);
        if (splittedIdentifier.size() > 1) {
            identifier = removeSpecialCharacters(identifier);
            splittedIdentifier.remove(identifier);
        } 
        
            System.out.println(identifier + " --> " + splittedIdentifier);
        return splittedIdentifier;
    }

    /**
     * Method that retrieves the words contained in all the string elements of a
     * Set of Strings
     *
     * @param sourceSet Set from which the words are to be retrieved
     * @return A set of words contained into the source list's contents
     */
    public TokenModel<String> retrieveWords(TreeSet<String> sourceSet) {
        TokenModel<String> tokenModel = new TokenModel();

        for (String string : sourceSet) {
            string = string.replaceAll("\\n", "");
            string = string.replaceAll("\\n", "");
            string = removeSpecialCharacters(string);
            TreeSet<String> words = splitIntoWords(string);
            System.out.println(string + " --> " + words);
            tokenModel.addTokens(words);
        }
//        
//        for(String token : tokenModel.getTokens()){
//            System.out.println("T: " + token + ": " + tokenModel.getTokenCount().get(token));
//        }

        return tokenModel;
    }

    /**
     * Method that retrieves the words inside a given string
     *
     * @param string to be splitted into words
     * @return A Set of words extracted from the input string
     */
    public TreeSet<String> splitIntoWords(String string) {
        TreeSet<String> splittedIdentifier = new TreeSet<>();
        splittedIdentifier.addAll(Arrays.asList(string.split(" ")));
        splittedIdentifier.remove("");
        return splittedIdentifier;
    }

    /**
     *
     * @param wordsSet
     * @param slotSize
     * @return
     */
    public HashSet<TreeSet<String>> divideIntoSlots(TreeSet<String> wordsSet, int slotSize) {
        HashSet<TreeSet<String>> slottedSet = new HashSet<>();
        int currentSlotSize = 0;
        TreeSet<String> slot = new TreeSet<>();
        for (String word : wordsSet) {
            if (currentSlotSize + word.length() > slotSize) {
                slottedSet.add(slot);
                slot = new TreeSet<>();
                currentSlotSize = 0;
            }
            slot.add(word);
            currentSlotSize += word.length();
        }
        slottedSet.add(slot);
        return slottedSet;
    }
}
