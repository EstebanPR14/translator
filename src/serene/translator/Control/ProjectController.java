/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package serene.translator.Control;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author ESTEBAN
 */
public class ProjectController {

    private String path;

    /**
     * Creates a new ProjectController object
     */
    public ProjectController() {
    }

    /**
     *
     * @param path
     */
    public ProjectController(String path) {
        this.path = path;
    }

    /**
     *
     * @return
     */
    public ArrayList<File> getProjectFilesList() {
        ArrayList<File> filesList = new ArrayList<>();
        FileManager fileManager = new FileManager();
        filesList = fileManager.getFilesInDirectory(path);
        return filesList;
    }
}
