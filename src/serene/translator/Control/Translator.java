/**
 * 
 */
package serene.translator.Control;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.translate.*;
import com.google.api.services.translate.Translate.*;
import com.google.api.services.translate.model.DetectionsResourceItems;
import com.google.api.services.translate.model.LanguagesResource;

/**
 * @author USER
 * 
 */
public class Translator {

	/**
	 * Be sure to specify the name of your application. If the application name
	 * is {@code null} or blank, the application will log a warning. Suggested
	 * format is "MyCompany-ProductName/1.0".
	 */
	private String applicationName;

	/** Set projectId to your Project ID from Overview pane in the APIs console */
	private String projectId;
	private String key;
	private Translate googleTranslate;
	private static HashMap<String, String> languages;

	/**
	 * 
	 * // Set up the HTTP transport and JSON factory
	 * 
	 * @param applicationName
	 * @param projectId
	 * @param key
	 */
	public Translator(String applicationName, String projectId, String key) {
		super();

		this.setApplicationName(applicationName);
		this.setProjectId(projectId);
		this.setKey(key);

		try {
			HttpTransport httpTransport = GoogleNetHttpTransport
					.newTrustedTransport();
			JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
			TranslateRequestInitializer keyInitializer = new TranslateRequestInitializer(
					key);
			googleTranslate = new Translate.Builder(httpTransport, jsonFactory,
					null).setApplicationName(this.applicationName)
					.setTranslateRequestInitializer(keyInitializer).build();

			System.out.println("Translate requ: "
					+ googleTranslate.getRootUrl());

		} catch (GeneralSecurityException | IOException e) {
			System.out.println("Google Translate API returned an error "
					+ e.getMessage());
			e.printStackTrace();
		}
		try {
			languages = getLanguages();
		} catch (IOException ex) {
			Logger.getLogger(Translator.class.getName()).log(Level.SEVERE,
					null, ex);
		}
	}

	/**
	 * Returns a list of supported languages with the language name in English
	 *
	 * @throws IOException
	 *             when the tranalation cannot be retrieved by the Google
	 *             Translate API
	 * @throws URISyntaxException
	 *             when the tranalation cannot be retrieved by the Google
	 *             Translate API
	 */
	public HashMap<String, String> getLanguages() throws IOException {
		Languages languagesList = googleTranslate.languages();
		List<LanguagesResource> languagesResources = languagesList.list()
				.setTarget("en").execute().getLanguages();

		HashMap<String, String> languagesMap = new HashMap<>();
		for (LanguagesResource languageResource : languagesResources) {
			languagesMap.put(languageResource.getLanguage(),
					languageResource.getName());
		}
		return languagesMap;
	}

	/**
	 *
	 * @param languageCode
	 * @return
	 */
	public String fullLanguageNameFromLanguageCode(String languageCode) {
		if (languageCode == null) {
			return "Not detected";
		}
		String languageName = languages.get(languageCode);
		return languageName;
	}

	/**
	 * @param query
	 * @param sourceLanguage
	 * @param targetLanguage
	 * @return
	 * @throws IOException
	 */
	public List translateText(String query, String sourceLanguage,
			String targetLanguage) throws IOException {
		// Translate "I" from unknown (auto-detect) to Spanish
		List<String> queryList = new ArrayList<>();
		queryList.add(query);
		Translate.Translations translations = googleTranslate.translations();
		Translate.Translations.List request = translations.list(queryList,
				targetLanguage);
		request.setSource(sourceLanguage);

		List translation = request.execute().getTranslations();
		System.out.println("en = '" + request.getQ() + " - " + translation
				+ "' in es");
		return translation;
		// OUTPUT: 'I' in en = 'Yo' in es
	}

	/**
	 * Translate from unknown (auto-detect) to target language
	 * 
	 * @param query
	 * @param targetLanguage
	 * @throws IOException
	 */
	public void translateFromUnknown(String query, String targetLanguage)
			throws IOException {
		Translate.Translations translations = googleTranslate.translations();
		List<String> queryList = new ArrayList<>();
		queryList.add(query);
		Translate.Translations.List request = translations.list(queryList,
				targetLanguage);
		List translation = request.execute().getTranslations();
		System.out.println("'I' in "// +
				// translation.getDetectedSourceLanguage()
				+ " = '" + translation + "' in " + targetLanguage);
		// OUTPUT: 'I' in no = 'En' in es
	}

	//
	/**
	 * Translate multiple source text strings
	 * 
	 * @param sourceTexts
	 */
	public void multipleSourceTexts(String[] sourceTexts) {
		String[] sourceTexts1 = { "I", "a" };
		// Translations[] translations = translator.translate(sourceTexts, null,
		// "es");
		// for (int i = 0, sourceTextsLength = sourceTexts.length; i <
		// sourceTextsLength; i++) {
		// System.out.println("'" + sourceTexts[i] + "' in en = " + "'"
		// + translations[i].getTranslatedText() + "' in es");
		// }
		// OUTPUT: 'I' in en = 'En' in es
		// 'a' in en = 'un' in es
	}

	/**
	 * @throws IOException
	 * 
	 */
	public List<List<DetectionsResourceItems>> detectLanguage(String[] elements)
			throws IOException {

		List<String> query = new ArrayList<>();
		for (int i = 0; i < elements.length; i++) {
			query.add(elements[i]);
		}
		Translate.Detections.List detections = googleTranslate.detections()
				.list(query);
		List<List<DetectionsResourceItems>> detectionResult = detections
				.execute().getDetections();
		System.out.println("detections = " + detectionResult.toString());
		// OUTPUT: detections = [[Detection{language='no', reliable=false,
		// confidence=0.09615925}], [Detection{language='en', reliable=false,
		// confidence=0.08430534}]]

		return detectionResult;
	}

	/**
	 * Method to detect the language of a given set of elements <br>
	 * OUTPUT form: <br>
	 * [[word], [Detection{language='no', reliable=false,
	 * confidence=0.09615925}]<br>
	 * [word], [Detection{language='en', reliable=false,
	 * confidence=0.08430534}]]
	 *
	 * @param elements
	 *            Array of elements for language detection
	 * @throws IOException
	 *             when the translation cannot be retrieved by the Google
	 *             Translate API
	 */
	public HashMap<String, DetectionsResourceItems> detectLanguagetoMap(
			String[] elements) throws IOException {
		List<String> query = new ArrayList<>();
		query.addAll(Arrays.asList(elements));
		Translate.Detections.List detections = googleTranslate.detections()
				.list(query);
		List<List<DetectionsResourceItems>> detectionResultList = detections
				.execute().getDetections();
		HashMap LanguageDetectionsMap = new HashMap();
		for (int i = 0; i < query.size(); i++) {
			System.out.println(query.get(i) + " --> "
					+ detectionResultList.get(i).get(0));
			LanguageDetectionsMap.put(query.get(i), detectionResultList.get(i)
					.get(0));
		}
		return LanguageDetectionsMap;
	}

	/**
	 * Method that generates a map with the number of word per language, from a
	 * set of language detection results
	 *
	 * @param languageDetectionMap
	 *            A map of language detection results
	 * @param wordCount
	 *            A map of words count
	 * @return A Map composed of language and number of words per language pairs
	 * @throws IOException
	 *             when the tranalation cannot be retrieved by the Google
	 *             Translate API
	 * @throws URISyntaxException
	 *             when the tranalation cannot be retrieved by the Google
	 *             Translate API
	 */
	public HashMap<String, Integer> wordsPerLanguageCount(
			HashMap<String, DetectionsResourceItems> languageDetectionMap,
			Map<String, Integer> wordCount) throws IOException,
			URISyntaxException {

		HashMap<String, Integer> wordsPerLanguage = new HashMap<>();

		for (String word : languageDetectionMap.keySet()) {
			DetectionsResourceItems detection = languageDetectionMap.get(word);
			String languageCode = detection.getLanguage();
			String language = fullLanguageNameFromLanguageCode(languageCode);
			if (wordsPerLanguage.containsKey(language)) {
				wordsPerLanguage.put(language, wordsPerLanguage.get(language)
						+ wordCount.get(word));
			} else {
				wordsPerLanguage.put(language, wordCount.get(word));
			}
		}
		return wordsPerLanguage;
	}

	/**
	 * Method that generates a map with the number of unique words per language,
	 * from a set of language detection results
	 *
	 * @param languageDetectionMap
	 *            A map of language detection results
	 * @return A Map composed of language and number of words per language pairs
	 * @throws IOException
	 *             when the tranalation cannot be retrieved by the Google
	 *             Translate API
	 * @throws URISyntaxException
	 *             when the tranalation cannot be retrieved by the Google
	 *             Translate API
	 */
	public HashMap<String, Integer> uniqueWordsPerLanguageCount(
			HashMap<String, DetectionsResourceItems> languageDetectionMap)
			throws IOException, URISyntaxException {

		HashMap<String, Integer> wordsPerLanguage = new HashMap<>();

		for (String word : languageDetectionMap.keySet()) {
			DetectionsResourceItems detection = languageDetectionMap.get(word);
			String languageCode = detection.getLanguage();
			String language = fullLanguageNameFromLanguageCode(languageCode);
			if (wordsPerLanguage.containsKey(language)) {
				wordsPerLanguage.put(language,
						wordsPerLanguage.get(language) + 1);
			} else {
				wordsPerLanguage.put(language, 1);
			}
		}
		return wordsPerLanguage;
	}

	/**
	 * Method that generates a set with the ratio of words per language from a
	 * Map containing the number of word per language
	 *
	 * @param wordsPerLanguageA
	 *            Map composed of language and number of words per language
	 *            pairs
	 * @return A Map composed of language and ratio of words per language pairs
	 */
	public HashMap<String, Double> languageRatio(
			HashMap<String, Integer> wordsPerLanguage) {
		HashMap<String, Double> languagesRatios = new HashMap<>();
		Integer totalWordCount = 0;
		for (Integer wordsCount : wordsPerLanguage.values()) {
			totalWordCount += wordsCount;
		}

		for (String language : wordsPerLanguage.keySet()) {
			double ratio = (double) wordsPerLanguage.get(language)
					/ totalWordCount;
			languagesRatios.put(language, ratio);
		}
		return languagesRatios;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
