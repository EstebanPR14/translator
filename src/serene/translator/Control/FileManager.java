package serene.translator.Control;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import serene.model.VideoMetadata;

/**
 * @author ESTEBAN
 * 
 */
public class FileManager {

	/**
	 * Creates a new FileManager object
	 */
	public FileManager() {
	}

	/**
	 * Saves the contents of the input data into a file with the specified path
	 *
	 * @param path
	 *            path to write into
	 * @param data
	 *            data to be written
	 */
	public void saveToFile(String path, String data) {
		File file = new File(path);
		Charset charset = Charset.forName("US-ASCII");

		try (BufferedWriter writer = Files.newBufferedWriter(file.toPath(),
				charset)) {
			writer.write(data, 0, data.length());
		} catch (IOException x) {
			System.err.format("IOException: %s%n", x);
		}

	}

	/**
	 * @param path
	 * @param data
	 */
	public <K, V> void saveToFile(String path, HashMap<K, V> data) {
		File file = new File(path);
		Charset charset = StandardCharsets.UTF_8;
		CharsetDecoder decoder = StandardCharsets.UTF_8.newDecoder();
		decoder.onMalformedInput(CodingErrorAction.IGNORE);
		try (BufferedWriter writer = Files.newBufferedWriter(file.toPath(),
				charset)) {
			for (K key : data.keySet()) {
				writer.write(key + "," + data.get(key));
				writer.newLine();
			}
			Desktop.getDesktop().open(new File(path));
		} catch (IOException x) {
			System.err.format("IOException: %s%n", x);
		}
	}

	/**
	 * @param filename
	 * @param data
	 */
	public void saveToFile(String filename, ArrayList<String> data) {
		File file = new File(filename);
		Charset charset = Charset.forName("UTF8");

		try (BufferedWriter writer = Files.newBufferedWriter(file.toPath(),
				charset)) {
			String dataItem;
			String newLine = System.getProperty("line.separator");
			while (!data.isEmpty()) {
				dataItem = data.remove(0) + newLine;
				writer.write(dataItem, 0, dataItem.length());
			}
		} catch (IOException x) {
			System.err.format("IOException: %s%n", x);
		}
		// Desktop d = Desktop.getDesktop();
		// try {
		// d.open(file);
		// } catch (IOException e) {
		// System.err.format("IOException: %s%n", e);
		// }

	}

	/**
	 * Returns the contents of the specified file as a list of Strings
	 *
	 * @param path
	 *            of the file to read
	 * @return A List of Strings with the contents of the given file
	 * @throws IOException
	 *             if there is an error accessing the specified file
	 */
	public List<String> readFileContent(String filename) throws IOException {

		File file = new File(filename);
		Charset charset = Charset.forName("UTF-8");
		List<String> lines = new LinkedList<>();
		try (BufferedReader reader = Files.newBufferedReader(file.toPath(),
				charset)) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				lines.add(line);
			}
		} catch (IOException x) {
			System.err.format("IOException: %s%n", x);
		}

		return lines;
	}

	/**
	 * Method to read the information of videos from a file, encoding it in the
	 * format of VideoMetadata objects
	 * 
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	public LinkedList<VideoMetadata> readFileContentMetadataFormat(
			String filename) throws IOException {

		LinkedList<VideoMetadata> videosMetadata = new LinkedList<>();
		File file = new File(filename);
		Charset charset = Charset.forName("UTF8");
		List<String> lines = new LinkedList<>();
		try (BufferedReader reader = Files.newBufferedReader(file.toPath(),
				charset)) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				lines.add(line);
			}
		} catch (IOException x) {
			System.err.format("IOException: %s%n", x);
		}

		VideoMetadata videoMetadata = new VideoMetadata();
		for (String line : lines) {
			String s = "id:___P7GFijzo";
			s.startsWith("id:");
			if (line.startsWith("id:")) {
				if (!videoMetadata.getTitle().equals("")) {
					videosMetadata.add(videoMetadata);
					videoMetadata = new VideoMetadata();
				}
				videoMetadata.setID(line.replace("id:", ""));
			} else {
				if (line.startsWith("title:")) {
					videoMetadata.setTitle(line.replace("title:", ""));
				} else {
					if (line.startsWith("description:")) {
						videoMetadata.setDescription(line.replace(
								"description:", ""));
					} else {
						if (!line.startsWith("count:")) {
							String description = videoMetadata.getDescription()
									.concat(" ").concat(line);
							videoMetadata.setDescription(description);
						}
					}
				}
			}
		}
		videosMetadata.add(videoMetadata);
		return videosMetadata;
	}

	/**
	 * Method that creates a new File instance by converting the given pathname
	 * string into an abstract pathname. If the given string is the empty
	 * string, then the result is the empty abstract pathname.
	 *
	 * @param path
	 *            path of the file
	 * @return an instance of type File
	 */
	public File getFile(String path) {
		File file = new File(path);
		return file;
	}

	/**
	 * Method that returns the content of a given file as a String
	 *
	 * @param file
	 *            a file
	 * @return the content of the file as a String
	 * @throws IOException
	 *             if there is an error opening the file
	 */
	public String getString(File file) throws IOException {
		StringBuilder fileData = new StringBuilder(1000);
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			char[] buf = new char[10];
			int numRead;
			while ((numRead = reader.read(buf)) != -1) {
				String readData = String.valueOf(buf, 0, numRead);
				fileData.append(readData);
				buf = new char[1024];
			}
		}
		return fileData.toString();
	}

	/**
	 * Method that retrieves all the files in the given directory and its
	 * subdrectories. This methos returns an empty Collection if the path is not
	 * a directory.
	 *
	 * @param path
	 *            Starting path to retrieve the files list
	 * @return An ArrayList of the files inside the directory and its
	 *         subdirectories
	 */
	public ArrayList<File> getFilesInDirectory(String path) {
		File file = new File(path);

		LinkedList<File> filesQueue = new LinkedList<>();
		if (file.isDirectory()) {
			filesQueue = new LinkedList<>(Arrays.asList(file.listFiles()));
		} else {
			filesQueue.add(file);

		}
		ArrayList<File> filesList = new ArrayList<>();

		while (!filesQueue.isEmpty()) {
			file = filesQueue.pollFirst();
			if (file.isDirectory()) {
				filesQueue.addAll(getFilesInDirectory(file.getPath()));
			} else {

				System.out.println("f: " + file.getName() + " - "
						+ file.getName().endsWith(".java"));
				if (file.getName().endsWith(".java")) {
					filesList.add(file);
				}
			}
		}
		return filesList;
	}

}
