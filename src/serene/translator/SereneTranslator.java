/**
 * 
 */
package serene.translator;

import com.google.api.services.translate.*;
import com.google.api.services.translate.model.DetectionsResourceItems;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

import serene.astclient.ASTClient;
import serene.model.VideoMetadata;
import serene.translator.Control.StringProcessor;
import serene.model.TokenModel;
import serene.translator.Control.FileManager;
import serene.translator.Control.Translator;

/**
 * Main class for the Serene Translator
 * 
 * @author ESTEBAN
 * 
 */
public class SereneTranslator {

	/**
	 * Be sure to specify the name of your application. If the application name
	 * is null or blank, the application will log a warning.".
	 */
	// Data with my main gmail
	// private static final String APPLICATION_NAME = "SereneCodeTranslator";

	/** Set projectId to your Project ID from Overview pane in the APIs console */
	// private static final String projectId = "serene-translator-751";
	// private static final String KEY =
	// "AIzaSyCHGIipFxTgl5rmlcKsqMcchZDfTfHszY8";
	// Key with my different email
	private static final String APPLICATION_NAME = "SereneTranslator";
	private static final String projectId = "serene-translator-002";
	private static final String KEY = "AIzaSyDRXCxF-Op5DlRnzFbP7cfMFgkHDFwzlkI";
	private static Translate googleTranslate;
	private static Translator translator;

	/**
	 * Creates a new SereneTranslator object
	 */
	public SereneTranslator() {
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		SereneTranslator sereneTranslator = new SereneTranslator();
		sereneTranslator.doStuff();

	}

	public void doStuff() {
		translator = new Translator(APPLICATION_NAME, projectId, KEY);

		// /Code for videos language detection

		String filename = "IDTitleDescription.txt";
		detectLanguageFromMetadata(filename);

		// Code for projects language detection

		// ASTClient astClient = new ASTClient();
		// try {
		// FileManager fileManager = new FileManager();

		// String filename = "ExporterToMsProjectFile.java";
		// File file = fileManager.getFile(filename);
		// System.out.println(ASTClient.clean(file));
		// System.out.println(fileManager.readFileContent(filename));
		// } catch (IOException/* | URISyntaxException */e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		// translator = new Translator(APPLICATION_NAME, projectId, KEY);

	}

	public static void detectLanguageFromMetadata(String filename) {
		try {
			System.out.println("Reading file");
			FileManager fileManager = new FileManager();
			LinkedList<VideoMetadata> videosMetadata = fileManager
					.readFileContentMetadataFormat(filename);
			System.out.println("Generating elements from metadata");
			ArrayList<String> elementsList = new ArrayList<>();
			for (VideoMetadata v : videosMetadata) {
				// System.out.println(v);
				elementsList.add(v.getTitle().concat(" ")
						.concat(v.getDescription()));
			}

			// Divide into sublists of size 100
			ArrayList<ArrayList<String>> chunks = new ArrayList<>();
			int chunkSize = 100;
			ArrayList<String> items = new ArrayList<String>();
			for (int i = 0; i < elementsList.size(); i++) {
				if (i % chunkSize == 0 && i != 0) {
					chunks.add(items);
					items = new ArrayList<String>();
				}
				items.add(elementsList.get(i));
			}
			if(elementsList.size()%chunkSize !=0){
				chunks.add(items);
			}
			System.out.println("chunks = " + chunks.size());
			int count = 0;
			for (ArrayList<String> item : chunks) {

				String[] elements = new String[item.size()];
				elements = item.toArray(elements);
				System.out.println("Processing with Google Translate");
				try {
					List<List<DetectionsResourceItems>> detectionResult = translator
							.detectLanguage(elements);

					System.out.println("Formatting results");
					ArrayList<String> results = new ArrayList<>();

					for (int index = 0; index < detectionResult.size(); index++) {
						DetectionsResourceItems detection = detectionResult
								.get(index).get(0);
						String result = videosMetadata.get(
								(count * 100) + index).getID()
								+ ","
								+ detection.getLanguage()
								+ ","
								+ detection.getConfidence();
						System.out.println(result);
						results.add(result);
					}
					System.out.println("Generating output file");
					String outputFilename = "LanguageDetections"
							+ (count + 1227) + ".txt";
					fileManager.saveToFile(outputFilename, results);
				} catch (Exception e) {
					System.out.println("Error on chunk " + (count+1227));
					e.printStackTrace();
				}
				count++;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Tests the Translator getLanguages() method.
	 *
	 * @throws URISyntaxException
	 *             In case of a malformed URI
	 * @throws IOException
	 *             In case of an HTTP exception
	 */
	public void testLanguages() throws IOException, URISyntaxException {
		HashMap languagesMap = translator.getLanguages();
		System.out.println("languages: " + languagesMap);
	}

	/**
	 * Tests the {@link Translator#translate(String, String, String)} and
	 * {@link Translator#translate(String[], String, String)} methods.
	 *
	 * @throws URISyntaxException
	 *             In case of a malformed URI
	 * @throws IOException
	 *             In case of an HTTP exception
	 */
	public void testTranslate() throws IOException {
		String query = "The blood of the wolf, Bellezza esiste negli occhi di contemplarla";
		String sourceLanguage = "it";
		String targetLanguage = "en";
		// translator.translateText(query, sourceLanguage, targetLanguage);
		// translator.translateFromUnknown(query, targetLanguage);
	}

	/**
	 * Tests the Translator.detectLanguage() method.
	 *
	 * @throws URISyntaxException
	 *             In case of a malformed URI
	 * @throws IOException
	 *             In case of an HTTP exception
	 */
	private void testDetect() throws IOException, URISyntaxException {
		StringProcessor stringProcessor = new StringProcessor();

		// String[] elements = new String[]{"The blood of the wolf",
		// "Bellezza esiste negli occhi di contemplarla",
		// "Me gustan los helados y los chocolates"};
		// TreeSet<String> elementsList = stringProcessor.retrieveWords(new
		// TreeSet(Arrays.asList(elements)));
		// elements = elementsList.toArray(elements);
		// List<List<DetectionsResourceItems>> detectLanguage =
		// translator.detectLanguage(elements);
		// HashMap<String, Integer> wordsPerLanguageCount =
		// translator.wordsPerLanguageCount(detectLanguage);
		// translator.languageRatio(wordsPerLanguageCount);
		// elements = new
		// String[]{"The blood of the wolf Bellezza esiste negli occhi di contemplarla"};
		// List<List<DetectionsResourceItems>> detectLanguage1 =
		// translator.detectLanguage(elements);
	}

	/**
	 * MEthod to test source code processing
	 */
	public void testSourceCodeProcessing() throws IOException,
			URISyntaxException {

		ASTClient astClient = new ASTClient();

		// Code to obtain the list of files on a directory

		FileManager fileManager = new FileManager();
		String DirectoryPath = "D:\\workspace\\PARSEC_Deployment_Package_v2";

		// String DirectoryPath = "D:\\workspace\\PARSEC_Deployment_Package_v2";
		// //PARSEC
		// String DirectoryPath = "D:\\workspace\\jzy3D\\trunk\\src"; //jzy3D
		// String DirectoryPath = "C:\\Users\\USER\\git\\waterloo\\Sources";
		// //waterloo
		// String DirectoryPath = "D:\\workspace\\graphstream\\trunk\\src";
		// //graphstream
		// String DirectoryPath = "D:\\workspace\\gral-core-0.10\\src"; //gral
		// String DirectoryPath = "D:\\workspace\\SweetHome3D-4.5-src\\src";
		// //SweetHome
		// String DirectoryPath =
		// "D:\\workspace\\LiveGraph.2.0.beta01.Complete\\org"; //LiveGraph
		// String DirectoryPath = "C:\\Users\\USER\\git\\surfaceplotter\\src";
		// //surfacePlotter
		// String DirectoryPath = "C:\\Users\\USER\\git\\JChart2D\\src";
		// //JChart2D
		// String DirectoryPath = "C:\\Users\\USER\\git\\JRobin\\src"; //JRobin
		// String DirectoryPath = "D:\\workspace\\XChart\\trunk\\xchart\\src";
		// //XChart
		// String DirectoryPath = "D:\\workspace\\grph"; //grph
		// String DirectoryPath = "D:\\workspace\\openchart2-1.4.3\\src";
		// //openchart2
		// String DirectoryPath = "D:\\workspace\\plot-1.6.1\\src"; //QNplot
		// String DirectoryPath = "D:\\workspace\\ptplot5.8"; //ptplot
		// String DirectoryPath = "D:\\workspace\\jcckit\\JCCKit\\jcckit";
		// //jcckit
		// String DirectoryPath =
		// "D:\\workspace\\jCharts-0.7.5\\jCharts-0.7.5\\src"; //jCharts
		// String DirectoryPath =
		// "D:\\workspace\\jgrapht-0.7.2\\jgrapht-0.7.2\\src\\org"; //jgrapht
		// String DirectoryPath = "D:\\workspace\\jheatchart-0.6\\src";
		// //jHeatChart
		// String DirectoryPath =
		// "D:\\workspace\\openthundergraph-0.37-beta\\openthundergraph-0.37-beta\\src";
		// //THUNDERGRAPH
		// String DirectoryPath = "D:\\workspace\\JUNG"; //JUNG
		// String DirectoryPath = "D:\workspace\Chart2D_1.9.6k\net"; //char2D
		// String DirectoryPath = "D:\workspace\egantt-0.5.3a\src"; //egantt
		// String DirectoryPath = "D:\\workspace\\weka\\src"; //weka
		// String DirectoryPath = "D:\\workspace\\jobimtext\\trunk"; //jobimtext
		// String DirectoryPath = "D:\\workspace\\jwordnet\\trunk\\jwnl\\src";
		// //JWordNet
		// String DirectoryPath = "D:\\workspace\\JModeller54b1\\src";
		// //JModeller
		// String DirectoryPath = "D:\\workspace\\srt-tran\\trunk\\src";
		// //srt-tran
		// String DirectoryPath = "D:\\workspace\\TML\\trunk"; //TML
		// String DirectoryPath =
		// "D:\\workspace\\TXM\\trunk\\Toolbox\\trunk\\org.textometrie.toolbox\\src\\java";
		// TXM core
		// String DirectoryPath = "C:\\Users\\USER\\git\\ProjectLibre";
		// //projectLibre
		// String DirectoryPath =
		// "C:\Users\USER\git\TemperatureConverter4Android\TempConverter\src";
		// //tempConverter
		// String DirectoryPath =
		// "C:\\Users\\USER\\git\\pi-physics\\sourceforge_pispace_core\\src\\pispace";
		// //pipyshics
		// String DirectoryPath = "C:\\Users\\USER\\git\\perceptron\\src";
		// //perceptron
		// String DirectoryPath = "C:\\Users\\USER\\git\\gannu"; //Gannu
		// String DirectoryPath =
		// "C:\\Users\\USER\\git\\EclipseCheckStylePlugin"; //Eclipse CheckStyle
		// Plugin
		// String DirectoryPath =
		// "C:\\Users\\USER\\git\\FreeMind\\freemind\\freemind"; //FreeMind
		// String DirectoryPath =
		// "C:\\Users\\USER\\git\\DutchSentimentAnalysis";
		// //DutchSentimentAnalysis
		// String DirectoryPath = "C:\\Users\\USER\\git\\AndEngine\\src";
		// //AndEngine
		// String DirectoryPath =
		// "D:\\workspace\\JHotDraw\\trunk\\jhotdraw7\\src"; //JHotDraw
		// String DirectoryPath =
		// "D:\\workspace\\game-off-2013.git\\trunk\\roomforchange";
		// //gameof2013
		// String DirectoryPath = "D:\\workspace\\mct.git\\trunk"; //mct
		// String DirectoryPath =
		// "D:\\workspace\\OpenLegislation.git\\trunk\\src"; //open legislation
		// String DirectoryPath =
		// "D:\\workspace\\morfologik-stemming.git\\trunk"; //morfologik
		// String DirectoryPath = "D:\\workspace\\EDNA1.3.1\\Source"; //EDNA
		// String DirectoryPath =
		// "D:\\workspace\\de.lorenz_fenster.SensorStream_IMU_GPS\\src";
		// //IMU-GPS
		// String DirectoryPath = "D:\\workspace\\DDrive Sources 2014-11-15";
		// //DDrive
		// String DirectoryPath =
		// "D:\\workspace\\Cost sensitive classifiers Code\\All Code\\CSC\\src\\csc";
		// //cost sensitive classifiers
		// String DirectoryPath = "D:\\workspace\\deTool_2.0.8_src"; //Detool
		// String DirectoryPath =
		// "D:\\workspace\\FALCON_SEARCH_V2.0\\USABLE\\classes"; //Falcon search
		// String DirectoryPath =
		// "D:\\workspace\\Mechaglot_Alpha3\\Mechaglot_Alpha3\\src"; //Mechaglot
		// String DirectoryPath = "D:\\workspace\\hfst"; //hsft
		// String DirectoryPath = "D:\\workspace\\wildfly.git"; //wildfly
		// String DirectoryPath = "D:\\workspace\\HermeneutiX\\src";
		// //HermeneutriX
		// String DirectoryPath = "D:\\workspace\\JHanNanum\\src"; // JHannanum
		// String DirectoryPath = "D:\\workspace\\Geosignature"; //GEoSignature
		// String DirectoryPath = "D:\\workspace\\ELIA\\src"; //ELIA
		// String DirectoryPath = "D:\\workspace\\mpc-hc.git\\src"; //media
		// player classic
		// String DirectoryPath = "D:\\workspace\\jmoney.git\\src"; //JMoney
		// String DirectoryPath =
		// "D:\\workspace\\jfreechart-code-3307-trunk\\source"; //JFreeChart
		// String DirectoryPath =
		// "D:\\workspace\\xradar-svn-1344-trunk\\xradar\\src"; //XRadar
		// String DirectoryPath = "D:\\workspace\\Heritrix\\src"; // Heritrix
		// String DirectoryPath = "D:\\workspace\\dependometer-java\\src";
		// //Dependometer
		// String DirectoryPath =
		// "D:\\workspace\\ganttproject\\ganttproject\\src"; // gantt Project
		// String DirectoryPath = "D:\\workspace\\elasticSearch\\main\\java";
		// //elastic search
		// String DirectoryPath =
		// "D:\\workspace\\adempiere\\contributions\\izpack\\src\\lib";
		// //Adempiere
		// String DirectoryPath = "D:\\workspace\\libgdx\\gdx\\src"; //Libgdx
		ArrayList<File> files = fileManager.getFilesInDirectory(DirectoryPath);

		// Code to process the content of the files, extracting a the set of
		// words (non repeated words are considered)
		TokenModel<String> tokens = new TokenModel<>();
		HashMap<String, DetectionsResourceItems> languageDetections = new HashMap<>(); // Language
																						// detections
		HashMap<String, Integer> wordsPerLanguageCount = new HashMap<>(); // Non
																			// unique
																			// words
																			// per
																			// language
																			// count
		HashMap<String, Integer> uniqueWordsPerLanguageCount = new HashMap<>(); // Unique
																				// words
																				// per
																				// language
																				// count
		try {
			for (File file : files) {
				tokens.addFromTokenModel(astClient.retrieveTokens(file));
				System.out.println("file: " + file.getName() + " \ncinfo: "
						+ tokens + "\n s: " + tokens.getTokens().size());
			}

			StringProcessor stringProcessor = new StringProcessor();
			HashSet<TreeSet<String>> slots = stringProcessor.divideIntoSlots(
					tokens.getTokens(), 128);
			int t = 0;
			// Detect language of the words in each slot and counts words per
			// language
			for (TreeSet<String> slot : slots) {
				t += slot.size();

				String[] elements = {};
				elements = slot.toArray(elements);
				HashMap<String, DetectionsResourceItems> languageDetectionMap = translator
						.detectLanguagetoMap(elements);
				languageDetections.putAll(languageDetectionMap);
				// Non - unique words count
				HashMap<String, Integer> newWordsPerLanguageCount = translator
						.wordsPerLanguageCount(languageDetectionMap,
								tokens.getTokenCount());
				mergeMaps(wordsPerLanguageCount, newWordsPerLanguageCount);
				// Unique words count
				HashMap<String, Integer> newUniqueWordsPerLanguageCount = translator
						.uniqueWordsPerLanguageCount(languageDetectionMap);
				mergeMaps(uniqueWordsPerLanguageCount,
						newUniqueWordsPerLanguageCount);

			}
		} catch (SocketTimeoutException ex) {
			System.out
					.println("Program terminated due to an error connecting to Google API services");
		} finally {
			HashMap<String, Double> nonUniqueLaunguageRatio = translator
					.languageRatio(wordsPerLanguageCount);
			HashMap<String, Double> uniqueLaunguageRatio = translator
					.languageRatio(uniqueWordsPerLanguageCount);

			fileManager
					.saveToFile("LanguageDetections.txt", languageDetections);

			fileManager.saveToFile("NonUniqueWordPerLanguageCount.txt",
					wordsPerLanguageCount);

			fileManager.saveToFile("UniqueWordPerLanguageCount.txt",
					uniqueWordsPerLanguageCount);

			fileManager.saveToFile("UniqueLanguageRatio.txt",
					uniqueLaunguageRatio);

			fileManager.saveToFile("NonuniqueLanguageRatio.txt",
					nonUniqueLaunguageRatio);
		}//

		//
	}

	private <K> HashMap<K, Integer> mergeMaps(HashMap<K, Integer> map,
			HashMap<K, Integer> otherMap) {
		for (K key : otherMap.keySet()) {
			if (map.containsKey(key)) {
				Integer value = map.get(key) + otherMap.get(key);
				map.put(key, value);
			} else {
				map.put(key, otherMap.get(key));
			}
		}
		System.out.println("MM: " + map);
		return map;
	}

	public static Translate getGoogleTranslate() {
		return googleTranslate;
	}

	public static void setGoogleTranslate(Translate googleTranslate) {
		SereneTranslator.googleTranslate = googleTranslate;
	}

}
