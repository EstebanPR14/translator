package serene.model;

public class VideoMetadata {

	private String ID;
	private String title;
	private String description;

	public VideoMetadata() {
		ID = "";
		this.title = "";
		this.description = "";
	}

	public VideoMetadata(String iD, String title, String description) {
		super();
		ID = iD;
		this.title = title;
		this.description = description;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "VideoMetadata [ID=" + ID + ", title=" + title
				+ ", description=" + description + "]";
	}
}
