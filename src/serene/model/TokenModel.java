/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package serene.model;

import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 *
 * @author USER
 */
public class TokenModel<E> {

    TreeSet<E> tokens = new TreeSet<>();
    TreeMap<E, Integer> tokenCount = new TreeMap<>();

    public TokenModel() {
        tokens = new TreeSet<>();
        tokenCount = new TreeMap<>();
    }

    /**
     *
     * @return
     */
    public TreeSet<E> getTokens() {
        return tokens;
    }

    /**
     *
     * @return
     */
    public TreeMap<E, Integer> getTokenCount() {
        return tokenCount;
    }

    /**
     *
     * @param token
     */
    public void addToken(E token) {
        tokens.add(token);
        if (tokenCount.containsKey(token)) {
            tokenCount.put(token, tokenCount.get(token) + 1);
        } else {
            tokenCount.put(token, 1);
        }
    }

    /**
     *
     * @param token
     * @param value
     */
    public void addToken(E token, Integer value) {
        tokens.add(token);
        tokenCount.put(token, value);
    }

    /**
     *
     * @param tokens
     */
    public void addTokens(Set<E> tokens) {
        for (E token : tokens) {
            addToken(token);
        }
    }

    /**
     *
     * @param tokenModel
     */
    public void addFromTokenModel(TokenModel<E> tokenModel) {
        for (E token : tokenModel.getTokens()) {
            if (tokens.contains(token)) {
                Integer j = tokenCount.get(token);
                Integer l = tokenModel.getTokenCount().get(token);
                Integer k = j + l;
                tokenCount.put(token, k);
            } else {
                addToken(token, tokenModel.getTokenCount().get(token));
            }
        }
    }

    @Override
    public String toString() {
        String toString = new String();
        for(E token : tokens){
            toString = toString + " T: " + token + " " + tokenCount.get(token);
        }
        return toString;
    }
}